-- Generate 1000 accounts
-- Balances: Random values in [1000, 5000] cents
-- IDs: serial values in [-499, 500]

INSERT INTO account_32 (id, balance_in_cents)
SELECT -500 + row_number() over (),
       (1000 + random() * 5000)::bigint
FROM generate_series(1, 1000);

INSERT INTO account_64 (id, balance_in_cents)
SELECT -500 + row_number() over (),
       (1000 + random() * 5000)::bigint
FROM generate_series(1, 1000);
