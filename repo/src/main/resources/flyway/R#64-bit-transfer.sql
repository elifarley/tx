/**
 * result: 0 = Success; >0 = Failure (do not retry); <0 = Temporary Failure (retry later)
 */
CREATE OR REPLACE FUNCTION transfer_64(amount_in_cents bigint, source_account bigint, target_account bigint,
                                       requester varchar(50))
    RETURNS TABLE
            (
                result                          INT,
                source_balance_after_withdrawal bigint
            )
    LANGUAGE plpgsql
    VOLATILE
AS
$SQL$
BEGIN
    RETURN QUERY
        with can_proceed(value) as (
            -- Avoid deadlocks by using TX advisory lock, which will be released upon leaving the implicit CTE transaction
            select source_account <> target_account
                       and amount_in_cents > 0
                       and pg_try_advisory_xact_lock(source_account # target_account) -- XOR
        ),

         withdraw as (
             update account_64 set balance_in_cents = balance_in_cents - amount_in_cents
                 where (select can_proceed.value from can_proceed)
                     and id = source_account and balance_in_cents >= amount_in_cents
                     and exists(select true from account_64 where id = target_account)
                 returning balance_in_cents
         ),

         deposit as (
             update account_64 set balance_in_cents = balance_in_cents + amount_in_cents
                 where id = target_account
                     and exists(select true from withdraw)
                 returning true
         ),

         log_it as (
             insert into account_log_64 (amount_in_cents, source_account, target_account, requester)
                 select amount_in_cents, source_account, target_account, requester
                 where exists(select true from deposit)
         )

        SELECT case
                   when source_account = target_account then 1 -- same accounts, do not retry
                   when amount_in_cents <= 0 then 3 -- amount must be positive, do not retry
                   when not (select can_proceed.value from can_proceed)
                       then -1 -- There's an ongoing TX between the same accounts, retry later
                   when not exists(select true from withdraw)
                       then 2 -- source_account or target_account not found, or insufficient funds in source_account, do not retry
                   when exists(select true from deposit) then 0
                   else 4 -- should never happen
                   end,
               (select balance_in_cents from withdraw);

end;
$SQL$;
