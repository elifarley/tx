CREATE TABLE IF NOT EXISTS account_32
(
    id               INT PRIMARY KEY,
    balance_in_cents BIGINT not null default 0
);

CREATE TABLE IF NOT EXISTS account_64
(
    id               BIGINT PRIMARY KEY, -- use snowflake-like algo to allow the app to choose new IDs
    balance_in_cents BIGINT not null default 0
);

CREATE TABLE IF NOT EXISTS account_log_32
(
    id              BIGSERIAL PRIMARY KEY,
    amount_in_cents BIGINT      NOT NULL,
    source_account  int         NOT NULL,
    target_account  int         NOT NULL,
    requester       varchar(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS account_log_64
(
    id              BIGSERIAL PRIMARY KEY,
    amount_in_cents BIGINT      NOT NULL,
    source_account  BIGINT      NOT NULL,
    target_account  BIGINT      NOT NULL,
    requester       varchar(50) NOT NULL
);
