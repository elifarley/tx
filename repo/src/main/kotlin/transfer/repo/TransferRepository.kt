package transfer.repo

import org.jooq.Record
import org.jooq.ResultQuery
import org.jooq.SQLDialect
import org.jooq.conf.Settings
import org.jooq.impl.DSL
import javax.sql.DataSource

sealed class TransferResult

data class TransferResultOk(val sourceBalanceAfterWithdrawal: Long) : TransferResult()

sealed class TransferResultError(val error: String) : TransferResult() {

    object SameAccounts : TransferResultError("Source and target accounts cannot be the same")

    object ConcurrentTransfer :
        TransferResultError("There's an ongoing transfer involving the same accounts, please retry later")

    object InsufficientFundsOrAccountNotFound : TransferResultError("Insufficient funds or account not found")

    object AmountIsNonPositive : TransferResultError("Amount must be positive")
}

private fun ResultQuery<Record>.handleResult(): TransferResult = fetchOneMap().let {
    when (val result: Int = it["result"] as Int) {
        0 -> TransferResultOk(it["source_balance_after_withdrawal"] as Long)
        1 -> TransferResultError.SameAccounts
        -1 -> TransferResultError.ConcurrentTransfer
        2 -> TransferResultError.InsufficientFundsOrAccountNotFound
        3 -> TransferResultError.AmountIsNonPositive
        else -> throw IllegalArgumentException("Invalid result: $result")
    }
}

class TransferRepository(ds: DataSource) {

    private val create = Settings().apply {
        withExecuteLogging(false)
    }.let { DSL.using(ds, SQLDialect.POSTGRES, it) }

    fun transfer32(amountInCents: Long, sourceAccount: Int, targetAccount: Int, requester: String): TransferResult =
        create.resultQuery(
            "select * from transfer_32(amount_in_cents := ?, source_account := ?, target_account := ?, requester := ?)",
            amountInCents, sourceAccount, targetAccount, requester
        ).handleResult()

    fun transfer64(amountInCents: Long, sourceAccount: Long, targetAccount: Long, requester: String): TransferResult =
        create.resultQuery(
            "select * from transfer_64(amount_in_cents := ?, source_account := ?, target_account := ?, requester := ?)",
            amountInCents, sourceAccount, targetAccount, requester
        ).handleResult()

    fun getBalanceInCentsFrom32bitAcoount(id: Int) = create.resultQuery(
        "select balance_in_cents from account_32 where id = ?",
        id
    ).fetchOne(0) as Long

    fun getBalanceInCentsFrom64bitAcoount(id: Long) = create.resultQuery(
        "select balance_in_cents from account_64 where id = ?",
        id
    ).fetchOne(0) as Long

    fun insertAccount32(id: Int, balanceInCents: Long) = create.query(
        "insert into account_32 (id, balance_in_cents) values (?, ?)",
        id, balanceInCents
    ).execute()

    fun insertAccount64(id: Int, balanceInCents: Long) = create.query(
        "insert into account_64 (id, balance_in_cents) values (?, ?)",
        id, balanceInCents
    ).execute()
}

