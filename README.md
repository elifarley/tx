# Transfer
You can transfer money from 2 accounts.
There are 2 different tables to hold accounts: account_32 (using 32bit ints as the primary key) and account_64 (using 64-bit ints for PK).
I'd suggest adopting the 32-bit version, unless we're talking about IoT accounts with more than 4 billion accounts. 

Ideally, in order to test the deadlock-free nature of this service, we should run it on a slow computer and then launch a stress test tool like Apache Bech (ab) in a few different computers.
Then, some of the requests would probably be served with an HTTP 503 SERVICE UNAVAILABLE response.


## How to run tests and build
```shell
mvn install
```

## How to Run

```shell
java -jar app/target/transfer-app-default-SNAPSHOT-jar-with-dependencies.jar
```

## How to get the Swagger API description
Run the server and visit http://localhost:8080/api/v1/swagger.json

## How to call the REST service
Examples:
```shell
# Transfer 3 cents from account 1 to account 2, using 32-bit account IDs
curl -XPOST -I 'http://localhost:8080/api/v1/accounts32/1/transfer-to/2?c=3'

# Transfer 3 cents from account 1 to account 2, using 64-bit account IDs
curl -XPOST -I 'http://localhost:8080/api/v1/accounts64/1/transfer-to/2?c=3'
```

## How to Configure

Please change app/src/main/resources/koin.properties
