package transfer

import transfer.repo.TransferRepository
import transfer.repo.TransferResult

interface TransferService<N : Number> {
    fun transfer(amountInCents: Long, sourceAccount: N, targetAccount: N, requester: String): TransferResult
}

class TransferService32(private val repository: TransferRepository) : TransferService<Int> {

    override fun transfer(
        amountInCents: Long,
        sourceAccount: Int,
        targetAccount: Int,
        requester: String
    ): TransferResult = repository.transfer32(amountInCents, sourceAccount, targetAccount, requester)
}

class TransferService64(private val repository: TransferRepository) : TransferService<Long> {

    override fun transfer(
        amountInCents: Long,
        sourceAccount: Long,
        targetAccount: Long,
        requester: String
    ): TransferResult = repository.transfer64(amountInCents, sourceAccount, targetAccount, requester)
}