package transfer

import klib.base.WithLogging
import klib.base.trimToDefault
import org.http4k.core.HttpHandler
import org.http4k.core.then
import org.http4k.filter.CorsPolicy
import org.http4k.filter.DebuggingFilters
import org.http4k.filter.ServerFilters
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.http4k.server.Http4kServer
import org.http4k.server.Netty
import org.http4k.server.ServerConfig
import org.http4k.server.asServer
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import org.koin.core.inject
import org.koin.core.qualifier.named
import org.koin.dsl.bind
import org.koin.dsl.module
import org.slf4j.LoggerFactory
import transfer.api.Api
import transfer.repo.TransferRepository
import javax.sql.DataSource

object AppRoutes : WithLogging() {

    operator fun invoke(
        transferService32: TransferService<Int>,
        transferService64: TransferService<Long>,
        debug: Boolean = false
    ): HttpHandler = routes(

        "/api/v1" bind Api.router(transferService32, transferService64)

    ).let {
        log.info("HTTP debugging is ${if (debug) "ENABLED" else "DISABLED"}")
        when {
            debug -> DebuggingFilters.PrintRequestAndResponse()
                .then(ServerFilters.Cors(CorsPolicy.UnsafeGlobalPermissive))
            else -> ServerFilters.Cors(CorsPolicy.UnsafeGlobalPermissive)
        }
            .then(ServerFilters.CatchAll())
            .then(it)
    }
}

class App : KoinComponent {

    private val appRestServer: Http4kServer by inject()
    private val ds: DataSource by inject()

    init {
        appRestServer.start().block()
    }
}

val remoteServerModule = module {
    single { Netty(getProperty("app.rest-port", 8080).toInt()) as ServerConfig }
}

val appModule = module {

    single { TransferRepository(get<DataSource>()) }
    single(named("transfer-service-32")) { TransferService32(get<TransferRepository>()) } bind TransferService::class
    single(named("transfer-service-64")) { TransferService64(get<TransferRepository>()) } bind TransferService::class

    single(named("app.routes")) {
        AppRoutes(
            get<TransferService<Int>>(named("transfer-service-32")),
            get<TransferService<Long>>(named("transfer-service-64")),
            System.getenv("DEBUG_HTTP").trimToDefault(getProperty("app.debug-http", "false")).toBoolean()
        )
    }
    single { get<HttpHandler>(named("app.routes")).asServer(get<ServerConfig>()) }

}

val mainAppModuleList = listOf(
    embeddedPGSQLModule, remoteServerModule, appModule
)

fun main(vararg args: String) {
    LoggerFactory.getLogger("AS").error("*****************")
    startKoin {
        printLogger()
        fileProperties()
        modules(mainAppModuleList)
    }
    App()
}
