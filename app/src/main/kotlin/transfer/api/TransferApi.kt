package transfer.api

import klib.base.WithLogging
import klib.http4k.toStackTraceResponse
import org.http4k.contract.ContractRoute
import org.http4k.contract.div
import org.http4k.contract.meta
import org.http4k.core.HttpHandler
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status.Companion.BAD_GATEWAY
import org.http4k.core.Status.Companion.BAD_REQUEST
import org.http4k.core.Status.Companion.FORBIDDEN
import org.http4k.core.Status.Companion.NO_CONTENT
import org.http4k.core.Status.Companion.SERVICE_UNAVAILABLE
import org.http4k.core.with
import org.http4k.lens.Header
import org.http4k.lens.Path
import org.http4k.lens.Query
import org.http4k.lens.int
import org.http4k.lens.long
import transfer.TransferService
import transfer.repo.TransferResult
import transfer.repo.TransferResultError
import transfer.repo.TransferResultOk

object TransferApi : WithLogging() {

    val queryAmountInCents = Query.long().required("c", "Amount (in cents)")
    private val pathSourceAccount32 = Path.int().of("source-account", "Source account (32 bit)")
    private val pathTargetAccount32 = Path.int().of("target-account", "Target account (32 bit)")
    private val pathSourceAccount64 = Path.long().of("source-account", "Source account (64 bit)")
    private val pathTargetAccount64 = Path.long().of("target-account", "Target account (64 bit)")
    private val headerUserAgent = Header.optional("user-agent")
    val headerNewBalanceInCents = Header.long().required("new-source-balance-in-cents")
    private val headerErrorReason = Header.required("error-reason")

    private fun <N : Number> preValidate(sourceAccount: N, targetAccount: N): TransferResultError? {
        if (sourceAccount == targetAccount) return TransferResultError.SameAccounts
        return null
    }

    fun routePOST32(
        transferService: TransferService<Int>
    ): ContractRoute {

        fun transferPOST(sourceAccount: Int, _dummy: String, targetAccount: Int): HttpHandler = { req: Request ->

            try {

                val amountInCents = queryAmountInCents(req)
                val requester = headerUserAgent(req) ?: ""

                log.debug(
                    "[32-bit transfer] {} cents [{} -> {}] ({})",
                    amountInCents, sourceAccount, targetAccount, requester
                )

                val transferResult = preValidate(sourceAccount, targetAccount) ?: transferService.transfer(
                    amountInCents, sourceAccount, targetAccount, requester
                )

                transferResult.asResponse()
            } catch (e: Exception) {
                log.error("[transferPOST 32-bit] Error", e)
                e.toStackTraceResponse()
            }

        }

        return "/accounts32" / pathSourceAccount32 / Path.fixed("transfer-to") / pathTargetAccount32 meta {
            summary = "Transfer cents between two accounts"
            queries += queryAmountInCents
            returning(
                "Transfer OK" to Response(NO_CONTENT).with(
                    headerNewBalanceInCents of 0L
                )
            )
            returning(TransferResultError.ConcurrentTransfer.error to Response(SERVICE_UNAVAILABLE))
            returning("Same accounts, insufficient funds or account not found" to Response(FORBIDDEN))
            returning("Dependency not working" to Response(BAD_GATEWAY))

        } bindContract Method.POST to ::transferPOST
    }

    fun routePOST64(
        transferService: TransferService<Long>
    ): ContractRoute {

        fun transferPOST(sourceAccount: Long, _dummy: String, targetAccount: Long): HttpHandler = { req: Request ->

            try {

                val amountInCents = queryAmountInCents(req)
                val requester = headerUserAgent(req) ?: ""

                log.debug(
                    "[64-bit transfer] {} cents [{} -> {}] ({})",
                    amountInCents, sourceAccount, targetAccount, requester
                )

                val transferResult = preValidate(sourceAccount, targetAccount) ?: transferService.transfer(
                    amountInCents, sourceAccount, targetAccount, requester
                )

                transferResult.asResponse()
            } catch (e: Exception) {
                log.error("[transferPOST 64-bit] Error", e)
                e.toStackTraceResponse()
            }

        }

        return "/accounts64" / pathSourceAccount64 / Path.fixed("transfer-to") / pathTargetAccount64 meta {
            summary = "Transfer cents between two accounts"
            queries += queryAmountInCents
            returning(
                "Transfer OK" to Response(NO_CONTENT).with(
                    headerNewBalanceInCents of 0L
                )
            )
            returning(TransferResultError.ConcurrentTransfer.error to Response(SERVICE_UNAVAILABLE))
            returning("Same accounts or non-positive amount" to Response(BAD_REQUEST))
            returning(TransferResultError.InsufficientFundsOrAccountNotFound.error to Response(FORBIDDEN))
            returning("Dependency not working" to Response(BAD_GATEWAY))

        } bindContract Method.POST to ::transferPOST
    }

    private fun TransferResult.asResponse() = when (this) {

        is TransferResultOk -> Response(NO_CONTENT).with(
            headerNewBalanceInCents of this.sourceBalanceAfterWithdrawal
        )

        TransferResultError.ConcurrentTransfer -> Response(SERVICE_UNAVAILABLE).with(
            headerErrorReason of TransferResultError.ConcurrentTransfer.error
        )

        TransferResultError.SameAccounts -> Response(BAD_REQUEST).with(
            headerErrorReason of TransferResultError.SameAccounts.error
        )

        TransferResultError.AmountIsNonPositive -> Response(BAD_REQUEST).with(
            headerErrorReason of TransferResultError.AmountIsNonPositive.error
        )

        TransferResultError.InsufficientFundsOrAccountNotFound -> Response(FORBIDDEN).with(
            headerErrorReason of TransferResultError.InsufficientFundsOrAccountNotFound.error
        )

    }
}