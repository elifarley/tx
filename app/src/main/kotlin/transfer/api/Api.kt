package transfer.api

import org.http4k.contract.contract
import org.http4k.contract.openapi.ApiInfo
import org.http4k.contract.openapi.v3.OpenApi3
import org.http4k.format.Jackson
import org.http4k.routing.RoutingHttpHandler
import transfer.TransferService

object Api {

    fun router(
        transferService32: TransferService<Int>,
        transferService64: TransferService<Long>
    ): RoutingHttpHandler =
        contract {
            renderer = OpenApi3(
                ApiInfo(
                    "Transfer API", "v1.0", """
            |Transfers money between two accounts.
            |An error is returned immediately in case of a possible deadlock.
            """.trimMargin()
                ), Jackson
            )
            descriptionPath = "/swagger.json"
            routes += TransferApi.routePOST32(transferService32)
            routes += TransferApi.routePOST64(transferService64)
        }
}
