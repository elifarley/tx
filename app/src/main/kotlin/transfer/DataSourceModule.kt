package transfer

import com.opentable.db.postgres.embedded.EmbeddedPostgres
import org.flywaydb.core.Flyway
import org.koin.dsl.module
import javax.sql.DataSource

private val embeddedPostgres: EmbeddedPostgres by lazy { EmbeddedPostgres.start() }

val embeddedPGSQLModule = module {

    single {
        embeddedPostgres.postgresDatabase!!.also {
            handleMigrations(it)
        }
    }

}

private fun handleMigrations(ds: DataSource) {
    Flyway.configure()
        .dataSource(ds)
        .sqlMigrationSeparator("#")
        .locations("flyway")
        .load()
        .migrate()
}
