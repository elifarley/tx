package transfer.test

import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.http4k.core.Status
import org.http4k.server.Http4kServer
import org.junit.After
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import org.koin.core.inject
import transfer.api.TransferApi.headerNewBalanceInCents
import transfer.appModule
import transfer.embeddedPGSQLModule
import transfer.repo.TransferRepository
import java.util.concurrent.Executors
import java.util.concurrent.atomic.AtomicInteger

class TransferTests : KoinComponent {

    companion object {

        var started = false
        var lastId: AtomicInteger = AtomicInteger(-499)

        @BeforeClass
        @JvmStatic
        fun setupClass() {
            startKoin {
                printLogger()
                fileProperties()
                modules(
                    listOf(
                        embeddedPGSQLModule,
                        selfRemoteServerModule,
                        appModule
                    )
                )
            }
        }
    }

    private val appRestServer: Http4kServer by inject()
    private val transferRepository: TransferRepository by inject()

    /**
     * If @param:sourceId is null, then source balance won't be asserted
     * If @param:targetId is null, then target balance won't be asserted
     * If amountInCents is 0, then it'll be asserted that both accounts have the same balance as before the transfer
     */
    private fun assertNewBalances(
        bits: ACCOUNT_BITS,
        sourceId: Int?,
        targetId: Int?,
        sourceBalance: Long = 0,
        targetBalance: Long = 0,
        amountInCents: Long = 0
    ) {

        assertThat(sourceId ?: targetId).`as`("At least one of sourceId and targetId must be non-null").isNotNull()

        // Fetch source and target balances from DB, if ID is not null
        val sourceBalanceAfterTransfer = sourceId?.let { transferRepository.getBalanceInCents(bits, it) }
        val targetBalanceAfterTransfer = targetId?.let { transferRepository.getBalanceInCents(bits, it) }

        sourceBalanceAfterTransfer?.let {
            assertThat(it).`as`("New source account balance, after transfer, as fetched from DB")
                .isEqualTo(sourceBalance - amountInCents)
        }

        targetBalanceAfterTransfer?.let {
            assertThat(targetBalanceAfterTransfer).`as`("New target account balance, after transfer, as fetched from DB")
                .isEqualTo(targetBalance + amountInCents)
        }
    }

    @Before
    @Synchronized
    fun setup() {
        if (!started) {
            appRestServer.start()
            started = true
        }
    }

    @After
    fun teardown() {
        assertThat(lastId.get()).`as`("The last ID must be within the limits of sample data").isLessThanOrEqualTo(500)
//        appRestServer.stop()
    }

    fun TransferRepository.getBalanceInCents(bits: ACCOUNT_BITS, id: Int) = when (bits) {
        ACCOUNT_BITS.B32 -> getBalanceInCentsFrom32bitAcoount(id)
        ACCOUNT_BITS.B64 -> getBalanceInCentsFrom64bitAcoount(id.toLong())
    }

    @Test
    fun `32bit Happy path - Transfering a valid amount between existing accounts`() = happyPath(ACCOUNT_BITS.B32)

    @Test
    fun `64bit Happy path - Transfering a valid amount between existing accounts`() = happyPath(ACCOUNT_BITS.B64)

    private fun happyPath(bits: ACCOUNT_BITS) {

        // GIVEN we have 2 accounts and their balances

        val id1 = lastId.getAndIncrement()
        val id2 = lastId.getAndIncrement()

        val sourceBalance = transferRepository.getBalanceInCents(bits, id1)
        val targetBalance = transferRepository.getBalanceInCents(bits, id2)

        // WHEN we transfer up to 1_000 cents
        val amountInCents: Long = 1_000
        selfClient.selfReqByBits(bits, amountInCents, id1.toLong(), id2.toLong())
            .let {

                // THEN

                assertThat(it.status).`as`("HTTP Status").isEqualTo(Status.NO_CONTENT)

                val sourceBalanceAfterTransfer = it.header(headerNewBalanceInCents.meta.name)?.toLongOrNull()

                assertThat(sourceBalanceAfterTransfer).`as`("New source account balance, after transfer, as sent in the HTTP response header")
                    .isEqualTo(sourceBalance - amountInCents)

                assertNewBalances(bits, id1, id2, sourceBalance, targetBalance, amountInCents)

            }
    }

    @Test
    fun `32bit Insufficient funds - Fail if transfer amount is higher than available at sourceAccount`() =
        insufficientFunds(ACCOUNT_BITS.B32)

    @Test
    fun `64bit Insufficient funds - Fail if transfer amount is higher than available at sourceAccount`() =
        insufficientFunds(ACCOUNT_BITS.B64)

    private fun insufficientFunds(bits: ACCOUNT_BITS) {

        // GIVEN we have 2 accounts and their balances

        val id1 = lastId.getAndIncrement()
        val id2 = lastId.getAndIncrement()

        val sourceBalance = transferRepository.getBalanceInCents(bits, id1)
        val targetBalance = transferRepository.getBalanceInCents(bits, id2)

        // WHEN we transfer more than 5000 cents
        selfClient.selfReqByBits(bits, 10_000, id1.toLong(), id2.toLong())
            .let {

                // THEN

                assertThat(it.status).`as`("HTTP Status").isEqualTo(Status.FORBIDDEN)

                assertNewBalances(bits, id1, id2, sourceBalance, targetBalance)

            }
    }

    @Test
    fun `32bit Account not found - Fail if any of the accounts is invalid`() =
        accountNotFound(ACCOUNT_BITS.B32)

    @Test
    fun `64bit Account not found - Fail if any of the accounts is invalid`() =
        accountNotFound(ACCOUNT_BITS.B64)

    private fun accountNotFound(bits: ACCOUNT_BITS) {

        val id = lastId.getAndIncrement()
        val balance = transferRepository.getBalanceInCents(bits, id)

        // WHEN we transfer up to 1_000 cents FROM invalid account
        selfClient.selfReqByBits(bits, 1, 501L, id.toLong())
            .let {

                // THEN

                assertThat(it.status).`as`("HTTP Status").isEqualTo(Status.FORBIDDEN)

                assertNewBalances(bits, null, id, 0, balance)

            }

        // WHEN we transfer up to 1_000 cents TO invalid account
        selfClient.selfReqByBits(bits, 1, id.toLong(), 501)
            .let {

                // THEN

                assertThat(it.status).`as`("HTTP Status").isEqualTo(Status.FORBIDDEN)

                assertNewBalances(bits, id, null, balance, 0)

            }
    }

    @Test
    fun `32bit Amount must be positive - Fail if it's ZERO or negative`() =
        amountIsNonPositive(ACCOUNT_BITS.B32)

    @Test
    fun `64bit Amount must be positive - Fail if it's ZERO or negative`() =
        amountIsNonPositive(ACCOUNT_BITS.B64)

    private fun amountIsNonPositive(bits: ACCOUNT_BITS) {

        // GIVEN we have 2 accounts and their balances

        val id1 = lastId.getAndIncrement()
        val id2 = lastId.getAndIncrement()

        val sourceBalance = transferRepository.getBalanceInCents(bits, id1)
        val targetBalance = transferRepository.getBalanceInCents(bits, id2)

        // WHEN we transfer 0 cents
        selfClient.selfReqByBits(bits, 0, id1.toLong(), id2.toLong())
            .let {

                // THEN

                assertThat(it.status).`as`("HTTP Status").isEqualTo(Status.BAD_REQUEST)

                assertNewBalances(bits, id1, id2, sourceBalance, targetBalance)

            }

        // WHEN we transfer -1 cent
        selfClient.selfReqByBits(bits, -1, id1.toLong(), id2.toLong())
            .let {

                // THEN
                assertThat(it.status).`as`("HTTP Status").isEqualTo(Status.BAD_REQUEST)

                assertNewBalances(bits, id1, id2, sourceBalance, targetBalance)
            }
    }

    @Test
    fun `32bit Accounts must be different - Fail if source and target accounts are the same`() =
        sameAccounts(ACCOUNT_BITS.B32)

    @Test
    fun `64bit Accounts must be different - Fail if source and target accounts are the same`() =
        sameAccounts(ACCOUNT_BITS.B64)

    private fun sameAccounts(bits: ACCOUNT_BITS) {

        // GIVEN we have 1 accounts and its balance

        val id = lastId.getAndIncrement()
        val sourceBalance = transferRepository.getBalanceInCents(bits, id)

        // WHEN we transfer to the same account

        selfClient.selfReqByBits(bits, 1, id.toLong(), id.toLong())
            .let {

                // THEN

                assertThat(it.status).`as`("HTTP Status").isEqualTo(Status.BAD_REQUEST)

                assertNewBalances(bits, id, null, sourceBalance, 0)

            }
    }

    fun `32bit deadlocks`() =
        deadLock(ACCOUNT_BITS.B32)

    fun `64bit deadlocks`() =
        deadLock(ACCOUNT_BITS.B64)

    private fun deadLock(bits: ACCOUNT_BITS) {

        // GIVEN we have 2 accounts and their balances

        val id1 = lastId.getAndIncrement().toLong()
        val id2 = lastId.getAndIncrement().toLong()

        val sourceBalance = transferRepository.getBalanceInCents(bits, id1.toInt())
        val targetBalance = transferRepository.getBalanceInCents(bits, id2.toInt())

        var serviceUnavailableCount = AtomicInteger(0)

        val context = Executors.newCachedThreadPool().asCoroutineDispatcher()
        runBlocking {
            repeat(200) {
                // launch a lot of coroutines
                launch(context) {
                    selfClient.selfReqByBits(bits, 1, id1, id2).let {
                        if (it.status == Status.SERVICE_UNAVAILABLE)
                            serviceUnavailableCount.incrementAndGet().let {
                                println("SERVICE_UNAVAILABLE: $it")
                            }
                    }
                }

                launch(context) {
                    selfClient.selfReqByBits(bits, 1, id2, id1).let {
                        if (it.status == Status.SERVICE_UNAVAILABLE)
                            serviceUnavailableCount.incrementAndGet().let {
                                println("SERVICE_UNAVAILABLE: $it")
                            }
                    }
                }

            }
        }

        assertThat(serviceUnavailableCount.get()).isNotEqualTo(0)
    }
}
