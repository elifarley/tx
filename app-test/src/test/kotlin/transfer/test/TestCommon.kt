package transfer.test

import klib.http4k.toAutoSetHost
import okhttp3.OkHttpClient
import org.assertj.core.api.SoftAssertions
import org.http4k.client.OkHttp
import org.http4k.contract.bindContract
import org.http4k.core.HttpHandler
import org.http4k.core.Method
import org.http4k.core.Method.GET
import org.http4k.core.Method.POST
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.with
import org.http4k.server.ServerConfig
import org.http4k.server.SunHttp
import org.koin.dsl.module
import transfer.api.TransferApi
import java.util.concurrent.TimeUnit

const val serverPort = 9900
const val servicePrefix = "/api/v1"

fun softAssert(b: SoftAssertions.() -> Unit) = SoftAssertions().apply(b).assertAll()

internal val selfRemoteServerModule = module {
    single { SunHttp(serverPort) as ServerConfig }
}

enum class ACCOUNT_BITS {
    B32, B64;
    override fun toString() = name.replace("B", "")
}

internal val selfClient: HttpHandler = ("http://localhost:$serverPort" to OkHttp(
    OkHttpClient.Builder().connectTimeout(300, TimeUnit.SECONDS).build()
)).toAutoSetHost

internal fun HttpHandler.selfReqByBits(bits: ACCOUNT_BITS, amountInCents: Long, sourceAccount: Long, targetAccount: Long) = selfReq(
    "/accounts$bits/$sourceAccount/transfer-to/$targetAccount", POST

) {
    it.with(TransferApi.queryAmountInCents of amountInCents)
}

internal fun HttpHandler.selfReq(
    path: String,
    method: Method = GET,
    reqLens: ((Request) -> Request)? = null
): Response = this(
    ("$servicePrefix$path" bindContract method).newRequest().let {
        if (reqLens != null) it.with(reqLens) else it
    }
)

